import React from 'react'
import { Card, Button, Badge } from 'react-bootstrap';

function Item(props) {
    return (
        <Card>
            <Card.Img variant="top" src={props.item.img} />
            <Card.Body>
                <Card.Title><h3>{props.item.title}</h3></Card.Title>
                <Card.Text>
                    Price: {props.item.price} $
                </Card.Text>

                
                <Button
                    variant="warning"
                    disabled={props.item.amount === 0}
                    onClick={() => {
                        props.onDelete(props.idx)
                    }}
                >Delete</Button>{" "}
                <Button
                    variant="success"
                    className="mx-2"
                    onClick={() =>
                        props.onAdd(props.idx)
                    }>Add</Button>{" "}

                <Button variant="info">{props.item.amount}</Button>

                <h4 className="my-2">Total: {props.item.total} $</h4>
            </Card.Body>
        </Card>
    )
}

export default Item
