import React from 'react'
import { Container, Table, Button } from 'react-bootstrap'

export default function TableData(fruits) {
    // console.log("Fruit", fruits);


    let temp = fruits.data.filter(item => {
        return item.amount > 0;
    })
    return (
        <Container>
            <br />
            <Button onClick={fruits.onclear} variant="danger">Clear All</Button>{' '}
            <Button variant="success">{temp.length} Foods</Button>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>No</th>
                        <th> Name</th>
                        <th>Quantity  </th>
                        <th>Price</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>

                    {temp.map((item, index) => (
                        <tr>
                            <td>{index + 1}</td>
                            <td>{item.title} </td>
                            <td>{item.amount}</td>
                            <td>{item.price} $</td>
                            <td>{item.total} $</td>
                            
                        </tr>
                    ))}

                    {/* <tr>
                        <td colSpan="4">Larry the Bird</td>
                        <td>@twitter</td>
                    </tr> */}
                </tbody>
            </Table>
        </Container>
    )
}

// export default TableData
