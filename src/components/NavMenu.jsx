import React from 'react'
import { Container, Navbar, Form, FormControl, Button, Nav } from 'react-bootstrap'

function NavMenu() {
    return (
        <Container>
            <Navbar bg="dark" variant="dark">
                <Navbar.Brand href="#home">KSHRD</Navbar.Brand>
                <Nav className="mr-auto">
                    <Nav.Link href="#home">Home</Nav.Link>
                    <Nav.Link href="#New">New</Nav.Link>
                    <Nav.Link href="#About">About us</Nav.Link>
                    <Nav.Link href="#Contact">Contact us</Nav.Link>
                </Nav>
                <Form inline>
                    <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                    <Button variant="outline-info">Search</Button>
                </Form>
            </Navbar>
            
        </Container>
    )
}

export default NavMenu
