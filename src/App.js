import './App.css';
import NavMenu from './components/NavMenu';
import React, { Component } from 'react'
import Content from './components/Content';
import TableData from './components/TableData';

export default class App extends Component {
  constructor() {
    super()
    this.state = {
      data: [
        {
          id: 1,
          title: "Coffee",
          amount: 0,
          img: 'image/coffee01.jpg',
          price: 5,
          total: 0,
        },
        {
          id: 2,
          title: "Fruit",
          amount: 0,
          img: "image/fruit01.jpg",
          price: 10,
          total: 0,
        },
        {
          id: 3,
          title: "Coffee",
          amount: 0,
          img: "image/coffee02.jpg",
          price: 6,
          total: 0,
        },
        {
          id: 4,
          title: "Fruit",
          amount: 0,
          img: "image/fruit02.jpg",
          price: 2,
          total: 0,
        }
      ]
    }
    this.onAdd = this.onAdd.bind(this)
    this.onDelete = this.onDelete.bind(this)
  }

  onAdd(index) {
    let data = this.state.data
    data[index].amount++
    data[index].total = data[index].amount * data[index].price
    this.setState({
      data: data
    })

  }
  onDelete(index) {
    let data = this.state.data
    data[index].amount--
    data[index].total = data[index].amount * data[index].price
    this.setState({
      data: data
    })
  }
 
  onclear = () => {
    let temp = [...this.state.data]
    temp.map(item=>{
      item.amount = 0
      item.total = 0
    })
    this.setState({
      data: temp
    })
  }

  render() {
    return (
      <div>
        <NavMenu />
        <Content
          onAdd={this.onAdd}
          onDelete={this.onDelete}
          items={this.state.data} />
        <TableData onclear={this.onclear} data={this.state.data}  /> 
      </div>
    )
  }
}

